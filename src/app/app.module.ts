import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import {FireBaseApiService} from "./fireBaseApi.service";


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [FireBaseApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
